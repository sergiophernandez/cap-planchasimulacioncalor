#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ADIABATICO -99.0
#define I_MAX 40
#define J_MAX 20

#define VALOR_0 0
#define VALOR_20 20
#define VALOR_100 100

#define NUM_ITERAS 2000

void matriscopy (void * destmat, void * srcmat);

int main()
{
	float tAnt[I_MAX][J_MAX];
	float *tempAnterior;
	float *pAnt;
	float t[I_MAX][J_MAX];
	float *temp;
	float *p;
	float *pAct;
	float *pAntAct;
	float r = 0.002;
	int i = 0;
	int j = 0;
	int h = 0;
	int k = 0;
	float evArr = 0;
	float evAba = 0;
	float evIzq = 0;
	float evDer = 0;
	int valor = 0;
	int itera = 0;
	
	tempAnterior = (float*)malloc(I_MAX*J_MAX*sizeof(float));
	pAnt=tempAnterior;
	
	temp = (float*)malloc(I_MAX*J_MAX*sizeof(float));
	p=temp;

	for(i=0;i<I_MAX;i++) {
		for(j=0;j<J_MAX;j++) {
			switch(j){
				case 0:
					valor = VALOR_0;
					break;
				case (J_MAX-1):
					valor = VALOR_100;
					break;
				case 3:
					if(i==1) valor = ADIABATICO;
					break;
				case 7:
					if(i==1) valor = ADIABATICO;
					break;
				case 11:
					if(i==1) valor = ADIABATICO;
					break;
				default:
					valor = VALOR_20;
					break;
			}
			tAnt[i][j] = t[i][j] = valor;
		}
	}
	
	i=j=0;
	h=0;
	for(i=0;i<I_MAX;i++) {
		for(j=0;j<J_MAX;j++) {
			temp[h]=tempAnterior[h]=tAnt[i][j];
			h++;
		}
	}
	
	i=j=0;
	
	while(itera < NUM_ITERAS) {
	
		for(i=0;i<I_MAX;i++) {
			for(j=0;j<J_MAX;j++) {
				
				if(j==0) {
					*(p+(i*J_MAX+j)) = 0.0; 
				} else if(j==J_MAX-1) {
					*(p+(i*J_MAX+j)) = 100.0;
				} else {
					if(*(p+(i*J_MAX+j)) != ADIABATICO) {
												
						if((i-1 >= 0) && (*(pAnt+((i-1)*J_MAX+j)) != ADIABATICO)) {
							evArr = r*(*(pAnt+(i*J_MAX+j))-*(pAnt+((i-1)*J_MAX+j)));
						} else {
							evArr = 0;
						}
						
						if((i+1 < I_MAX) && (*(pAnt+((i+1)*J_MAX+j)) != ADIABATICO)) {
							evAba = r*(*(pAnt+(i*J_MAX+j))-*(pAnt+((i+1)*J_MAX+j)));
						} else {
							evAba = 0;
						}
						
						if((j-1 >= 0) && (*(pAnt+(i*J_MAX+(j-1))) != ADIABATICO)) {
							evIzq = r*(*(pAnt+(i*J_MAX+j))-*(pAnt+(i*J_MAX+(j-1))));
						} else {
							evIzq = 0;		
						}
						
						if((j+1 < J_MAX) && (*(pAnt+(i*J_MAX+(j+1))) != ADIABATICO)) {
							evDer = r*(*(pAnt+(i*J_MAX+j))-*(pAnt+(i*J_MAX+(j+1))));
						} else {
							evDer = 0;		
						}
						
						*(p+(i*J_MAX+j)) = *(pAnt+(i*J_MAX+j)) - evArr - evAba - evIzq - evDer;
					}
				}
			}			
		}
			
		printf("== VALORES DE T ==\n");
		for(h=0;h<I_MAX;h++) {
			for(k=0;k<J_MAX;k++) {
				printf("%8.3f",*(p+(h*J_MAX+k)));
			}
			printf("\n");
		}
		itera++;
		i=j=0;
		p=pAnt;
	}
	
    return 0;
}

void matriscopy (void * destmat, void * srcmat) 
{
  memcpy(destmat,srcmat, I_MAX*J_MAX*sizeof(int));
}
