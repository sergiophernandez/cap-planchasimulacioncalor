#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <sys/time.h>

#define ADIABATICO -99.0
#define I_MAX 6
#define J_MAX 10

#define VALOR_0 0
#define VALOR_20 20
#define VALOR_100 100

#define NUM_ITERAS 20000

int main(int argc, char  **argv)
{
	float *pAnt;
	float *temp;
	float *p;
	float *porc;
	float r = 0.002;
	int i = 0;
	int j = 0;
	int evaluarJ = 0;
	int iniEvaluarJ = 0;
	float evArr = 0;
	float evAba = 0;
	float evIzq = 0;
	float evDer = 0;
	int valor = 0;
	int itera = 0;
	
	/** Variables de MPI */
	int num_procs, my_rank;
	
	int *reparto;
	int *desplzs;
	
	int cols = J_MAX;
	int rows = I_MAX;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
	MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
	
	int elem_x_proc = J_MAX*I_MAX/num_procs;
	
	if (my_rank==0)
		p = (float*)malloc(I_MAX*J_MAX*sizeof(float));
		
	
	MPI_Bcast(&cols, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&rows, 1, MPI_INT, 0, MPI_COMM_WORLD);
    
	reparto = (int*)malloc(num_procs*sizeof(int));
	for(i=0;i<num_procs;i++){
		if(i==0 || i==num_procs-1){
			reparto[i]=J_MAX*I_MAX/num_procs+J_MAX;
		} else {
			reparto[i]=J_MAX*I_MAX/num_procs+(2*J_MAX);
		}
	}
	
	desplzs = (int*)malloc(num_procs*sizeof(int));
	for(i=0;i<num_procs;i++){
		if(i==0){
			desplzs[i]=0;
		} else {
			desplzs[i]=desplzs[i-1]+(i*J_MAX*I_MAX/num_procs-J_MAX);
		}
	}
	
	printf("Hello from processor %d of %d\n", my_rank+1,num_procs);
	
	if(my_rank == 0) {
		for(i=0;i<I_MAX;i++) {
			for(j=0;j<J_MAX;j++) {
				switch(j){
					case 0:
						valor = VALOR_0;
						break;
					case (J_MAX-1):
						valor = VALOR_100;
						break;
					case 3:
						if(i==1) valor = ADIABATICO;
						break;
					case 7:
						if(i==1) valor = ADIABATICO;
						break;
					case 11:
						if(i==1) valor = ADIABATICO;
						break;
					default:
						valor = VALOR_20;
						break;
				}
				p[i*J_MAX+j] = valor;
			}
		}
	} // fin if
	
	/** variable donde se almacena el trozo */
	porc = malloc(sizeof(float)*(reparto[my_rank]));
	pAnt = malloc(sizeof(float)*(reparto[my_rank]));
	
	//enviamos a todos su porcion
	if(my_rank == 0){
		MPI_Scatter(p, elem_x_proc, MPI_FLOAT, porc, elem_x_proc, MPI_FLOAT,0,MPI_COMM_WORLD);
	} else if(my_rank == num_procs-1) {
		MPI_Scatter(p, elem_x_proc, MPI_FLOAT, porc+J_MAX, elem_x_proc, MPI_FLOAT,0,MPI_COMM_WORLD);
	} else {
		MPI_Scatter(p, elem_x_proc, MPI_FLOAT, porc+J_MAX, elem_x_proc, MPI_FLOAT,0,MPI_COMM_WORLD);
	}
	
	
	//enviamos las filas que falten a cada uno
	if(my_rank == 0) {
		for(i=0;i<J_MAX;i++) {
			porc[elem_x_proc+i]=p[elem_x_proc+i];
		}
		for(i=1;i<num_procs-1;i++) {
			MPI_Send(p+desplzs[i],J_MAX,MPI_FLOAT,i,0,MPI_COMM_WORLD);
			MPI_Send(p+desplzs[i]+J_MAX+elem_x_proc,J_MAX,MPI_FLOAT,i,0,MPI_COMM_WORLD);
		}
		MPI_Send(p+desplzs[i],J_MAX,MPI_FLOAT,num_procs-1,0,MPI_COMM_WORLD);
	} else if (my_rank == num_procs - 1) {
		MPI_Recv(porc,J_MAX,MPI_FLOAT,0,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
	} else {
		MPI_Recv(porc,J_MAX,MPI_FLOAT,0,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		MPI_Recv(porc+J_MAX+elem_x_proc,J_MAX,MPI_FLOAT,0,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
	}
	
	//hacemos copia de porc a pAnt
	for(i=0;i<reparto[my_rank];i++) {
		pAnt[i]=porc[i];
	}
	
	//establecemos los rangos de cálculo dentro de cada porcion
	if (my_rank == 0) {
		evaluarJ = I_MAX/num_procs;
		iniEvaluarJ = 0;
	} else if (my_rank == num_procs-1) {
		evaluarJ = I_MAX/num_procs+1;
		iniEvaluarJ = 1;
	} else {
		evaluarJ = I_MAX/num_procs+1;
		iniEvaluarJ = 1;
	}
	
	
	/** Se realizan los cáluclos necesarios */
	while(itera < NUM_ITERAS) {
	
		for(i=iniEvaluarJ;i<evaluarJ;i++) {
			for(j=0;j<J_MAX;j++) {
				if(j==0) {
					*(porc+(i*J_MAX+j)) = 0.0; 
				} else if(j==J_MAX-1) {
					*(porc+(i*J_MAX+j)) = 100.0;
				} else {
					if(*(porc+(i*J_MAX+j)) != ADIABATICO) {
												
						if((i-1 >= 0) && (*(pAnt+((i-1)*J_MAX+j)) != ADIABATICO)) {
							evArr = r*(*(pAnt+(i*J_MAX+j))-*(pAnt+((i-1)*J_MAX+j)));
						} else {
							evArr = 0;
						}
						
						if((i+1 < evaluarJ) && (*(pAnt+((i+1)*J_MAX+j)) != ADIABATICO)) {
							evAba = r*(*(pAnt+(i*J_MAX+j))-*(pAnt+((i+1)*J_MAX+j)));
						} else {
							evAba = 0;
						}
						
						if((j-1 >= 0) && (*(pAnt+(i*J_MAX+(j-1))) != ADIABATICO)) {
							evIzq = r*(*(pAnt+(i*J_MAX+j))-*(pAnt+(i*J_MAX+(j-1))));
						} else {
							evIzq = 0;		
						}
						
						if((j+1 < J_MAX) && (*(pAnt+(i*J_MAX+(j+1))) != ADIABATICO)) {
							evDer = r*(*(pAnt+(i*J_MAX+j))-*(pAnt+(i*J_MAX+(j+1))));
						} else {
							evDer = 0;		
						}
						
						*(porc+(i*J_MAX+j)) = *(pAnt+(i*J_MAX+j)) - evArr - evAba - evIzq - evDer;
					}
				}
			}			
		}//fin for
		
		/** 
		 * Enviar a los procesos necesarios mi calculo y esperar a recibir el de los demas
		 */
		if(my_rank == 0) {
			MPI_Send(porc+reparto[my_rank]-(2*J_MAX),J_MAX,MPI_FLOAT,1,0,MPI_COMM_WORLD);
			MPI_Recv(porc+reparto[my_rank]-J_MAX,J_MAX,MPI_FLOAT,1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		} else if(my_rank == num_procs-1) {
			MPI_Send(porc+J_MAX,J_MAX,MPI_FLOAT, my_rank-1,0,MPI_COMM_WORLD);
			MPI_Recv(porc,J_MAX,MPI_FLOAT,my_rank-1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		} else {
			MPI_Send(porc+J_MAX,J_MAX,MPI_FLOAT,my_rank-1,0,MPI_COMM_WORLD);
			MPI_Send(porc+reparto[my_rank]-(2*J_MAX),J_MAX,MPI_FLOAT,my_rank+1,0,MPI_COMM_WORLD);
			MPI_Recv(porc,J_MAX,MPI_FLOAT,my_rank-1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
			MPI_Recv(porc+reparto[my_rank]-J_MAX,J_MAX,MPI_FLOAT,my_rank+1,0,MPI_COMM_WORLD,MPI_STATUS_IGNORE);
		}
		
		for(i=0;i<reparto[my_rank];i++) {
			pAnt[i]=porc[i];
		}
		
		itera++;
		
	} //fin while
	
	//recogemos la porcion de todos	
	if (my_rank==0) {
		MPI_Gather(porc, J_MAX*I_MAX/num_procs, MPI_FLOAT, p, J_MAX*I_MAX/num_procs, MPI_FLOAT, 0, MPI_COMM_WORLD);
	} else if(my_rank==num_procs-1) {
		MPI_Gather(porc+J_MAX, J_MAX*I_MAX/num_procs, MPI_FLOAT, p, J_MAX*I_MAX/num_procs, MPI_FLOAT, 0, MPI_COMM_WORLD);
	} else {
		MPI_Gather(porc+J_MAX, J_MAX*I_MAX/num_procs, MPI_FLOAT, p, J_MAX*I_MAX/num_procs, MPI_FLOAT, 0, MPI_COMM_WORLD);
	}
	
	if(my_rank == 0) {
		printf("== VALORES DE T ==\n");
		for(i=0;i<I_MAX;i++) {
			for(j=0;j<J_MAX;j++) {
				printf("%8.3f",*(p+(i*J_MAX+j)));
			}
			printf("\n");
		}
	}
	  
	printf("Tiempo de ejecucion de %d = %e\n",my_rank+1, MPI_Wtime());
	
	MPI_Finalize();
    return 0;
}
